//import the User model:
const User = require("../models/User");

module.exports.createUserController = (req,res) => {

	console.log(req.body);//contain the details to be included in our document

	//We should not allow users to have the same username. Thus, we will add a validation.
	//Check if there is a document who share the same username as our input:
	//Model.findOne() is a mongoose method which is similar to db.collection.findOne() in MongoDB
	//However, with Model.findOne(), we cna process the result via our api:
	//findOne document which username property matches with the username provided in the request body.
	//if findOne() cannot find a document that matches you criteria, it will return null.
	User.findOne({username: req.body.username})
	.then(result => {

		//console.log(result);
		//console log the result of findOne(). If findOne() cannot find a matching document, it will return null.
		//If findOne() is able to find a document that matched our criteria it will return the document. The document is an object.
		if(result !== null && result.username === req.body.username){

			//return keyword is added here to end the process and the code block.
			//the execution the statement will end at res.send();
			return res.send("Duplicate User Found");

		} else {

					//newUser object should contain the details to be saved as a new User document
					let newUser = new User({

						username: req.body.username,
						password: req.body.password

					})

					//save() method is from an object created by a model.
					//save() method will allow us to save our new user object into the users collection.
					//.then() will allow us to process the result of .save() in a new function. This time sending the result to the client using res.send()
					//.catch() allows us to process the errors separately from our results. This time, sending the error to the client.

					newUser.save()
					.then(result => res.send(result))
					.catch(error => res.send(error));

		}


	})
	.catch(error => res.send(error));

}

module.exports.getAllUsersController = (req,res) => {

	//Model.find() is a method from our model. This will allow us to find documents from the collection that the model is connected with.
	//mongoDB this may look like this: db.users.find()
	//If the find() method is left with an empty criteria {}, what documents are we getting?
	//All documents in the collection will returned.
	//then() will send all the documents in the collection to the client.
	//catch() will catch any error that may occur while searching for documents.
	User.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error))

};

