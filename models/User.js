const mongoose = require("mongoose");

//Schema
const userSchema = new mongoose.Schema({

	username: String,
	password: String

});


//Model
//mongoose.model(<nameOfCollectionInAtlas>,<schemaToFollow>)
//The first argument in the model() is the collection the model is connected to.
//MongoDB automatically changes the name you give into plural

module.exports = mongoose.model("User",userSchema);
