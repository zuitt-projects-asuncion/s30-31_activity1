const express = require("express");
const router = express.Router();

const userControllers = require("../controllers/singleUserController");


router.put('/:id',userControllers.updateUserNameController);

module.exports = router;

