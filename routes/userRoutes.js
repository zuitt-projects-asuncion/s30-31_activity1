const express = require("express");
const router = express.Router();

//import userControllers
const userControllers = require("../controllers/userControllers");

//Create a new user
router.post('/', userControllers.createUserController);

//GET all users documents from our users collection
router.get('/',userControllers.getAllUsersController);

module.exports = router;
